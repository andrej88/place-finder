import sveltePreprocess from 'svelte-preprocess';

const production = process.env.NODE_WATCH === 'production';

const preprocess = sveltePreprocess({
    typescript: {
        tsconfigFile: './tsconfig.json'
    },
});

export default {
    dev: !production,
    preprocess
};
