
export function shuffleWeighted<T>(array: T[], weightField: (item: T) => number): T[] {
    if (array.length === 0)
        return array;
    let result = [];
    let len = array.length;
    let totalWeights = array.map(weightField).reduce((a, b) => a + b);
    for (let i = 0; i < len; i++) {
        let randomWeightValue = Math.random() * totalWeights;
        let weightedRandomIndex = -1;
        for (let j = 0; j < array.length; j++) {
            let item = array[j];
            if (weightField(item) > randomWeightValue) {
                weightedRandomIndex = j;
                break;
            }
            randomWeightValue -= weightField(item);
        }
        let item = array.splice(weightedRandomIndex, 1)[0];
        totalWeights -= weightField(item);
        result.push(item);
    }
    return result;

}

export function shuffle<T>(input: T[]): T[] {
    for (let i = input.length - 1; i >= 0; i--) {

        const randomIndex = Math.floor(Math.random() * (i + 1));
        const itemAtIndex = input[randomIndex];

        input[randomIndex] = input[i];
        input[i] = itemAtIndex;
    }
    return input;
}
