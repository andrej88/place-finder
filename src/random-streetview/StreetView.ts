import { shuffle, shuffleWeighted } from 'src/util/shuffle';
import { CoverageCache } from './cache';

export interface Tile {
    x: number
    y: number
    zoom: number
}

interface TileWithCoverage {
    x: number
    y: number
    zoom: number
    sv: number
    photo: number
}

interface TileWithCoverageAndImage {
    x: number
    y: number
    zoom: number
    sv: number
    photo: number
    img?: HTMLImageElement
}

export default class StreetView {

    slowCpu: boolean;
    coverageCache: CoverageCache;
    canvas: HTMLCanvasElement;
    context: CanvasRenderingContext2D;
    enableCaching: boolean;
    cacheKey: string;
    polygon: google.maps.Polygon;
    google: GoogleNamespace;
    area: number;
    smallestContainingTile: { x: number; y: number; zoom: number; };
    typeColors: { color: number[]; id: 'sv' | 'photo'; }[];
    distribution: 'weighted' | 'uniform';

    constructor(
        polygon: google.maps.Polygon,
        enableCaching: boolean,
        cacheKey: string,
        google: GoogleNamespace
    ) {
        this.slowCpu = false;
        this.coverageCache = new CoverageCache();
        this.coverageCache.loadFromStorage();

        this.canvas = document.createElement("canvas");
        this.context = this.canvas.getContext("2d");
        //google maps coverage images are 256x256
        this.canvas.width = 256;
        this.canvas.height = 256;
        this.enableCaching = enableCaching;
        this.cacheKey = cacheKey;
        this.polygon = polygon;
        this.google = google;
        this.smallestContainingTile = this.polygonToSmallestContainingTile(polygon);

        let area = 0;
        if (polygon) {
            // No fold method so this will have to do.
            polygon.getPaths().forEach(path => {
                area += this.google.maps.geometry.spherical.computeArea(path);
            });
        }
        this.area = area;

        this.typeColors = [
            { color: [84, 158, 185], id: 'sv' },            // This is the coarse-grained coverage color. It comes with various alpha values.
            { color: [161, 228, 242, 255], id: 'sv' },      // This is the border color of the fine-grained coverage lines. The fill color happens to be the border color of photosphere dots, but the border color is streetview-only.
            { color: [14, 73, 232, 132], id: 'sv' },        // This is the color of the coverage lines on the mts1.google.apis.com endpoint.
            { color: [202, 241, 208, 106], id: 'photo' },   // This is the fill color of the photosphere dots.
        ];
    }

    async randomValidLocation({
        endZoom = 13,
        type = 'sv',
        distribution = 'weighted'
    }: {
        endZoom: number;
        type: 'sv' | 'photo' | 'both';
        distribution: 'weighted' | 'uniform';
    }): Promise<google.maps.LatLng> {
        this.distribution = distribution;

        let tile = await this.randomValidTile(endZoom, type, this.smallestContainingTile);
        if (!tile)
            return null;
        let canvas = document.createElement("canvas");
        let context = canvas.getContext("2d");
        if (!tile.img)
            tile.img = await this.getTileImage(tile);
        let img = tile.img;
        canvas.width = img.width;
        canvas.height = img.height;
        context.drawImage(img, 0, 0);

        let data = context.getImageData(0, 0, img.width, img.height).data;

        let pixelCounts = { count: 0, indices: [] };
        for (let i = 0; i < data.length; i += 4) {
            let color = data.slice(i, i + 4);
            let colorType = this.getColorType(color);
            if (colorType === type || (colorType !== 'empty' && type === 'both')) {
                pixelCounts.count++;
                pixelCounts.indices.push(i);
            }
        }

        if (pixelCounts.count === 0) {
            console.error("No blue pixel found");
            return this.randomValidLocation({ endZoom, type, distribution });
        }
        let randomSvPixel = Math.floor(Math.random() * pixelCounts.count);
        let randomSvIndex = pixelCounts.indices[randomSvPixel];
        let x = (randomSvIndex / 4) % img.width;
        let y = Math.floor((randomSvIndex / 4) / img.width);

        if (this.enableCaching)
            this.coverageCache.saveToStorage();

        return this.tilePixelToCoordinate(tile, x, y);
    }

    containsLocation(location: google.maps.LatLng | google.maps.LatLngLiteral, polygon: google.maps.Polygon): boolean {
        if (!polygon)
            return true;
        return this.google.maps.geometry.poly.containsLocation(location, polygon);
    }

    async waitSleep(time: number): Promise<void> {
        return new Promise(resolve => {
            setTimeout(resolve, time);
        });
    }

    async randomValidTile(
        endZoom: number,
        type: 'sv' | 'photo' | 'both',
        chosenTile: Tile = { x: 0, y: 0, zoom: 0 },
        startZoom: number = chosenTile.zoom
    ): Promise<{
        sv?: number
        photo?: number
        img?: HTMLImageElement
        x: number
        y: number
        zoom: number
    } | null> {
        if (chosenTile.zoom >= endZoom) {
            return chosenTile;
        }
        const photoSphereZoomLevel = 12;

        let subTiles = await this.getSubTiles(chosenTile);

        let validTiles = subTiles
            .filter(tile =>
                type === 'sv' && tile.sv > 0 ||
                type === 'photo' && tile.photo > 0 ||
                type === 'both' && (tile.photo > 0 || tile.sv > 0) ||
                //When under photosphere zoom level, also consider sv tiles valid tiles, because photospheres aren't visible yet
                tile.zoom <= photoSphereZoomLevel && tile.sv > 0)
            .filter(tile => this.tileIntersectsMap(tile));


        if (chosenTile.zoom === startZoom && validTiles.length === 0 && chosenTile.zoom <= 7) {
            //OH OH SPAGHETTIOS
            //Can't find anything in the start tile, trying to go ahead by ignoring street view coverage
            validTiles = subTiles
                .filter(tile => this.tileIntersectsMap(tile));
            startZoom = validTiles[0].zoom;
        }

        const shuffleFun =
            this.distribution === 'uniform' ?
                shuffle :
                (array: TileWithCoverage[]) =>
                    shuffleWeighted(
                        array,
                        item => (
                            (chosenTile.zoom + 1 <= photoSphereZoomLevel) ?
                                item.sv + item.photo :
                                item[type]
                        )
                    );
        let shuffledTiles = shuffleFun(validTiles);

        for (let tile of shuffledTiles) {
            let subTile = await this.randomValidTile(endZoom, type, tile, startZoom);

            if (subTile !== null && (subTile.sv > 0 || subTile.photo > 0))
                return subTile;
        }
        return null;
    }

    tileEquals(tileA: { x: number; y: number; zoom: number; }, tileB: { x: number; y: number; zoom: number; }): boolean {
        return (tileA.x === tileB.x && tileA.y === tileB.y && tileA.zoom === tileB.zoom);
    }

    getTileCornerCoordinates(tile: Tile): [google.maps.LatLng, google.maps.LatLng, google.maps.LatLng, google.maps.LatLng] {
        return [
            this.tilePixelToCoordinate(tile, 0, 0),// top left
            this.tilePixelToCoordinate(tile, 256, 0),// top right
            this.tilePixelToCoordinate(tile, 256, 256),// bottom right
            this.tilePixelToCoordinate(tile, 0, 256),// bottom left
        ];
    }

    tileIntersectsMap(tile: Tile): boolean {
        if (!this.polygon)
            return true;

        let tileCoordinates = this.getTileCornerCoordinates(tile);
        //Check if tile corners are in map bounds
        for (let coordinate of tileCoordinates)
            if (this.containsLocation(coordinate, this.polygon)) {
                return true;
            }

        //Maybe one of the 4 tile corners don't intersect, doesn't mean the two polygons don't intersect
        let mapsBounds = new this.google.maps.LatLngBounds();
        for (let coordinate of tileCoordinates)
            mapsBounds.extend(coordinate);

        // Check if map coordinates are in within tile bounds
        let mapContains = false;

        // No fold method so this will have to do.
        this.polygon.getPaths().forEach(path => {
            path.forEach(point => {
                if (mapsBounds.contains(point))
                    mapContains = true;
            });
        });

        return mapContains;
    }

    async getSubTiles(tile: Tile): Promise<TileWithCoverageAndImage[]> {
        //Zooming multiplies coordinates by 2 (4 sub tiles in a tile)
        let startX = tile.x * 2;
        let startY = tile.y * 2;
        let endX = startX + 2;
        let endY = startY + 2;

        return this.getTileGrid(startX, endX, startY, endY, tile.zoom + 1);
    }

    async getTileGrid(startX: number, endX: number, startY: number, endY: number, zoom: number): Promise<TileWithCoverageAndImage[]> {

        const tasks: Promise<TileWithCoverageAndImage>[] = [];

        for (let y = startY; y < endY; y++)
            for (let x = startX; x < endX; x++)
                tasks.push(this.getTile({ x, y, zoom }));

        return await Promise.all(tasks);
    }

    tilePixelToCoordinate(tile: Tile, pixelX: number, pixelY: number): google.maps.LatLng {
        let x = tile.x + pixelX / 256;
        let y = tile.y + pixelY / 256;

        x *= 2 ** (8 - tile.zoom);
        y *= 2 ** (8 - tile.zoom);

        let lng = x / 256 * 360 - 180;
        let n = Math.PI - 2 * Math.PI * y / 256;
        let lat = (180 / Math.PI * Math.atan(0.5 * (Math.exp(n) - Math.exp(-n))));

        return new this.google.maps.LatLng(lat, lng);
    }

    toRadians(degrees: number): number {
        return degrees * Math.PI / 180;
    }

    polygonToBounds(polygon: google.maps.Polygon): google.maps.LatLngBounds {
        const bounds = new this.google.maps.LatLngBounds();
        polygon.getPaths().forEach(path => {
            path.forEach(pos => {
                bounds.extend(pos);
            });
        });
        return bounds;
    }

    polygonToSmallestContainingTile(polygon: google.maps.Polygon): { x: number; y: number; zoom: number; } {
        if (!polygon)
            return { x: 0, y: 0, zoom: 0 };
        let bounds = this.polygonToBounds(polygon);
        let ne = bounds.getNorthEast();
        let sw = bounds.getSouthWest();
        let startZoom = 0;
        let endZoom = 18;
        let resultTile = { x: 0, y: 0, zoom: startZoom };
        for (let zoom = startZoom; zoom <= endZoom; zoom++) {
            let neTile = this.coordinateToTile(ne, zoom);
            let swTile = this.coordinateToTile(sw, zoom);
            let equals = this.tileEquals(neTile, swTile);
            if (!equals)
                break;
            resultTile = neTile;
        }
        return resultTile;
    }

    coordinateToTile(coordinate: google.maps.LatLng, zoom: number): { x: number; y: number; zoom: number; } {
        let latRad = this.toRadians(coordinate.lat());
        let n = 2.0 ** zoom;
        let xTile = Math.floor((coordinate.lng() + 180.0) / 360.0 * n);
        let yTile = Math.floor((1.0 - Math.log(Math.tan(latRad) + (1 / Math.cos(latRad))) / Math.PI) / 2.0 * n);
        return { x: xTile, y: yTile, zoom };
    }



    async getTileImage(tile: Tile): Promise<HTMLImageElement> {
        return new Promise(async resolve => {
            const url = getUrl(tile);
            let response = await fetch(url);
            let blob = await response.blob();
            const img = new Image();
            // document.querySelector('.temp').prepend(img);
            img.src = URL.createObjectURL(blob);
            img.onload = () => resolve(img);
        });
    }

    async getTile(tile: Tile): Promise<TileWithCoverageAndImage> {
        return new Promise(async resolve => {
            if (this.coverageCache.contains(this.cacheKey, tile)) {
                let { sv, photo } = this.coverageCache.get(this.cacheKey, tile);
                resolve({
                    sv, photo, ...tile
                });
                return;
            }
            let img = await this.getTileImage(tile);
            let c = await this.getTileCoverage(tile, img);
            this.coverageCache.set(this.cacheKey, tile, c);
            let { sv, photo } = this.coverageCache.get(this.cacheKey, tile);
            resolve({
                sv, photo, img, ...tile
            });
        });
    }

    getColorType(rgba: Uint8ClampedArray): 'sv' | 'photo' | 'empty' {
        if (rgba[2] === 0)
            return 'empty';

        const allowedColorDiff = 4;
        typeLoop:
        for (let { id, color } of this.typeColors) {
            for (let i = 0; i < color.length; i++) {
                const componentDifference = Math.abs(color[i] - rgba[i]);
                if (componentDifference > allowedColorDiff)
                    continue typeLoop;
            }
            return id;
        }
        return 'empty';
    }

    isTileFullyContainedInMap(tile: Tile): boolean {
        let coordinates = this.getTileCornerCoordinates(tile);
        for (let coordinate of coordinates) {
            if (!this.containsLocation(coordinate, this.polygon))
                return false;
        }
        return true;
    }

    async getTileCoverage(tile: Tile, img: HTMLImageElement): Promise<[number, number]> {
        this.context.clearRect(0, 0, this.canvas.width, this.canvas.height);
        this.context.drawImage(img, 0, 0);
        let data = this.context.getImageData(0, 0, img.width, img.height).data;
        let svCoverage = 0;
        let photoCoverage = 0;
        let isFullyContained = !!this.polygon && this.isTileFullyContainedInMap(tile);

        //asia area:           87868883173444
        //spain area:          680475474716
        //EU area:             12047591207736
        //russia area:         16934010870404
        let massiveArea = /**/ 5000000000000;
        let bigArea = /*    */ 1000000000000;

        let chunkSize = 16;
        if (tile.zoom <= 2 && this.area < massiveArea)//0, 1, 2
            chunkSize = 4;
        else if (tile.zoom <= 4 && this.area < massiveArea)//3
            chunkSize = 4;
        else if (tile.zoom <= 7 && this.area < bigArea)
            chunkSize = 8;

        let pixelChunkSize;
        if (tile.zoom <= 6)
            pixelChunkSize = 16;
        else if (tile.zoom <= 7)
            pixelChunkSize = 16;
        else if (tile.zoom <= 8)
            pixelChunkSize = 8;
        else if (tile.zoom <= 9)
            pixelChunkSize = 4;
        else
            pixelChunkSize = 2;

        pixelChunkSize = Math.min(pixelChunkSize, chunkSize);

        for (let y = 0; y < img.height; y += chunkSize) {
            for (let x = 0; x < img.width; x += chunkSize) {
                if (this.slowCpu)
                    await this.waitSleep(10);
                if (!isFullyContained) {
                    let coordinate = this.tilePixelToCoordinate(tile, x + chunkSize / 2, y + chunkSize / 2);
                    if (!this.containsLocation(coordinate, this.polygon)) {
                        continue;
                    }
                    // console.log("Chunk is in polygon!");
                }
                for (let pY = y + pixelChunkSize / 2; pY < y + chunkSize; pY += pixelChunkSize) {
                    for (let pX = x + pixelChunkSize / 2; pX < x + chunkSize; pX += pixelChunkSize) {
                        // console.log(pX, pY);
                        let i = (pY * img.width + pX) * 4;
                        let color = data.slice(i, i + 4);
                        let colorType = this.getColorType(color);
                        if (colorType === 'sv')
                            svCoverage++;
                        if (colorType === 'photo')
                            photoCoverage++;
                    }
                }
            }
        }
        return [svCoverage, photoCoverage];
    }
}

function getUrl(tile: Tile): URL {
    return new URL(
        // This one includes imagery on lh3.ggpht.com, which gives a CORS error:
        // `https://maps.googleapis.com/maps/vt?pb=!1m5!1m4!1i${tile.zoom}!2i${tile.x}!3i${tile.y}!4i256!2m8!1e2!2ssvv!4m2!1scb_client!2sapiv3!4m2!1scc!2s*211m3*211e3*212b1*213e2*211m3*211e2*212b1*213e2!3m3!3sUS!12m1!1e68!4e0`
        // This one does not have that imagery
        `https://mts1.googleapis.com/vt?hl=en-US&lyrs=svv|cb_client:apiv3&style=40,18&x=${tile.x}&y=${tile.y}&z=${tile.zoom}`
    );
}
