import StreetView from "./StreetView";

export class RandomStreetView {

    _streetView: StreetView;
    endZoom: number;
    type: 'sv' | 'photo' | 'both';
    distribution: 'weighted' | 'uniform';

    constructor({
        polygon = undefined,
        enableCaching = true,
        endZoom = 14,
        cacheKey = undefined,
        type = 'sv',
        distribution = 'weighted',
        google,
    }: {
        polygon?: number[][][] | google.maps.Polygon;
        enableCaching?: boolean;
        endZoom?: number;
        cacheKey?: string;
        type?: 'sv' | 'photo' | 'both';
        distribution?: 'weighted' | 'uniform';
        google: GoogleNamespace;
    }) {
        if (endZoom < 11)
            console.warn("endZoom parameter should not be less than 11");
        else if (endZoom > 22)
            console.error("endZoom can't be higher than 22");

        let polygonObject: google.maps.Polygon

        if (polygon instanceof Array) {
            let paths = polygon.map(path => path.map(([lat, lng]) => new google.maps.LatLng(lat, lng)));
            polygonObject = new google.maps.Polygon({
                paths: paths,
                strokeColor: "#00ff7a",
                strokeOpacity: 0.8,
                strokeWeight: 2,
                fillColor: "#00ff7a",
                fillOpacity: 0.35,
                draggable: false,
                clickable: false,
            });
        } else {
            // It's null or already a  google.maps.Polygon
            polygonObject = polygon
        }

        if (enableCaching && !cacheKey) {
            if (polygon) {
                cacheKey = '';
                // No fold method so this will have to do.
                polygonObject.getPaths().forEach(p => p.forEach(c => cacheKey += c.lat().toString() + c.lng()));
            } else {
                cacheKey = 'globe';
            }
        }

        this._streetView = new StreetView(polygonObject, enableCaching, cacheKey, google);
        this.endZoom = endZoom;
        this.type = type;
        this.distribution = distribution;
    }

    async getRandomLocations(nLocations: number, onLocation: (location: [number, number]) => void = () => 0): Promise<[number, number][]> {
        const get = async () => {
            let location = await this.getRandomLocation();
            onLocation(location);
            return location;
        };

        const tasks = [];
        for (let i = 0; i < nLocations; i++)
            tasks.push(get());

        return await Promise.all(tasks);
    }

    async getRandomLocation(): Promise<[number, number]> {
        const location = await this._streetView.randomValidLocation({
            endZoom: this.endZoom,
            distribution: this.distribution,
            type: this.type
        });
        if (location === null)
            return null;
        return [location.lat(), location.lng()];
    }
}
