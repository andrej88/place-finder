The files in this folder are modified versions of those found here:
https://github.com/RuurdBijlsma/random-streetview/tree/540421c99055aae18448d7de1873e153ed7fe70a

For an overview of how tiles work in Google Maps, see here:
https://www.maptiler.com/google-maps-coordinates-tile-bounds-projection/
