import type { Tile } from "./StreetView";

type CoverageCacheStructure = {
    [cacheKey: string]: {
        [zoom: number]: {
            [x: number]: {
                [y: number]: [number, number]
            }
        }
    }
};

export class CoverageCache {
    cache: CoverageCacheStructure

    loadFromStorage() {
        const tileCoverageCache = localStorage.getItem('tileCoverage');
        this.cache = tileCoverageCache === null ? {} : JSON.parse(tileCoverageCache);
    }

    saveToStorage() {
        localStorage.tileCoverage = JSON.stringify(this.cache);
    }

    contains(cacheKey: string, tile: Tile): boolean {
        return (
            this.cache[cacheKey] &&
            this.cache[cacheKey][tile.zoom] &&
            this.cache[cacheKey][tile.zoom][tile.x] &&
            this.cache[cacheKey][tile.zoom][tile.x][tile.y] &&
            true
        );
    }

    get(cacheKey: string, tile: Tile): {
        sv: number
        photo: number
    } {
        let [svCoverage, photoCoverage] = this.cache[cacheKey][tile.zoom][tile.x][tile.y];
        return {
            sv: svCoverage,
            photo: photoCoverage,
        }
    }

    set(cacheKey: string, tile: Tile, value: [number, number]) {
        if (!this.cache[cacheKey])
            this.cache[cacheKey] = {};
        if (!this.cache[cacheKey][tile.zoom])
            this.cache[cacheKey][tile.zoom] = {};
        if (!this.cache[cacheKey][tile.zoom][tile.x])
            this.cache[cacheKey][tile.zoom][tile.x] = {};
        this.cache[cacheKey][tile.zoom][tile.x][tile.y] = value;
    }
}
