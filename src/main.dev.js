import './app.css'
import App from './App.svelte'
import 'svelte-devtools-standalone'

const app = new App({
  target: document.getElementById('app')
})

export default app
