export function getRandomLatLon() {
    for (let i = 0; i < 10000; i++) {
        const latitude =
            90 * ((2 / Math.PI) * Math.acos(2 * Math.random() - 1) - 1);
        const longitude = 180 * (Math.random() * 2 - 1);
        return [latitude, longitude]
    }
}
