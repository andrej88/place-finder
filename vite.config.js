import { defineConfig } from 'vite'
import { svelte } from '@sveltejs/vite-plugin-svelte'
import * as path from 'path'
import { ViteEjsPlugin } from "vite-plugin-ejs";

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [
    svelte(),
    ViteEjsPlugin(),
  ],
  resolve: {
    alias: {
      'src': path.resolve(__dirname, 'src')
    }
  }
})
